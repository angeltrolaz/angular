import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // Agregado

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { InicioUsuarioComponent } from './inicio-usuario/inicio-usuario.component';
import { EdificiosEstructurasComponent } from './edificios-estructuras/edificios-estructuras.component';
import { FormularioEdificioComponent } from './formulario-edificio/formulario-edificio.component';
import { FacultadesComponent } from './facultades/facultades.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InicioUsuarioComponent,
    EdificiosEstructurasComponent,
    FormularioEdificioComponent,
    FacultadesComponent,
    ConfiguracionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule // Agregado
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
