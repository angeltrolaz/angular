import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.css']
})
export class ConfiguracionComponent {
  @ViewChild('opcion') opcionSelect!: ElementRef<HTMLSelectElement>;
  @ViewChild('campoUsuario') campoUsuario!: ElementRef<HTMLInputElement>;
  @ViewChild('campoContra') campoContra!: ElementRef<HTMLInputElement>;
  @ViewChild('campoNombresCompletos') campoNombresCompletos!: ElementRef<HTMLInputElement>;
  @ViewChild('campoEmail') campoEmail!: ElementRef<HTMLInputElement>;
  @ViewChild('campoTelefono') campoTelefono!: ElementRef<HTMLInputElement>;
  @ViewChild('campoDireccion') campoDireccion!: ElementRef<HTMLInputElement>;

  validarRes(): boolean {
    const opcionSeleccionada = this.opcionSelect.nativeElement.value;

    switch (opcionSeleccionada) {
      case 'usuario':
        const usuario = this.campoUsuario.nativeElement.value.trim();
        if (!this.validarUsuario(usuario)) {
          alert('Por favor, ingrese un nombre de usuario válido (solo letras, números, puntos, guiones bajos y guiones medios)');
          return false;
        }
        break;
      case 'contra':
        const contra = this.campoContra.nativeElement.value.trim();
        if (!this.validarContra(contra)) {
          alert('Por favor, ingrese una contraseña válida (mínimo 8 caracteres, al menos una letra mayúscula, una letra minúscula, un número y un carácter especial)');
          return false;
        }
        break;
      case 'nombrescompletos':
        const nombresCompletos = this.campoNombresCompletos.nativeElement.value.trim();
        if (!this.validarNombresCompletos(nombresCompletos)) {
          alert("Por favor ingrese un nombre válido con 4 palabras separadas por un espacio, cada una empezando con una letra mayúscula y seguida de letras minúsculas");
          return false;
        }
        break;
      case 'email':
        const email = this.campoEmail.nativeElement.value.trim();
        if (!this.validarEmail(email)) {
          alert('Por favor, ingrese una dirección de correo electrónico válida');
          return false;
        }
        break;
      case 'telefono':
        const telefono = this.campoTelefono.nativeElement.value.trim();
        if (!this.validarTelefono(telefono)) {
          alert('Por favor, ingrese un número de teléfono válido (10 dígitos)');
          return false;
        }
        break;
      case 'direccion':
        const direccion = this.campoDireccion.nativeElement.value.trim();
        if (!this.validarDireccion(direccion)) {
          alert('Por favor, ingrese una dirección válida (solo letras, números y algunos caracteres especiales)');
          return false;
        }
        break;
    }

    return true;
  }

  private validarUsuario(usuario: string): boolean {
    const regexUsuario = /^[a-zA-Z0-9._-]+$/;
    return regexUsuario.test(usuario);
  }

  private validarContra(contra: string): boolean {
    const regexContra = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!%*?&]{8,}$/;
    return regexContra.test(contra);
  }

  private validarNombresCompletos(nombresCompletos: string): boolean {
    const regexNombres = /^[A-Z][a-z]+\s[A-Z][a-z]+\s[A-Z][a-z]+\s[A-Z][a-z]+$/;
    return regexNombres.test(nombresCompletos);
  }

  private validarEmail(email: string): boolean {
    const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regexEmail.test(email);
  }

  private validarTelefono(telefono: string): boolean {
    const regexTelefono = /^\d{10}$/;
    return regexTelefono.test(telefono);
  }

  private validarDireccion(direccion: string): boolean {
    const regexDireccion = /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\s#.,-]+$/;
    return regexDireccion.test(direccion);
  }
}
