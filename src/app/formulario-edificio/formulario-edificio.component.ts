import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-formulario-edificio',
  templateUrl: './formulario-edificio.component.html',
  styleUrls: ['./formulario-edificio.component.css']
})
export class FormularioEdificioComponent implements OnInit {
  rutaImagen: string = '';
  edificioId: string = '';
  edificio: { ID: string, descripcion: string, valor: string, depreciacion: string, mantenimiento: string } | undefined;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.rutaImagen = params['rutaImagen'];
      this.edificioId = params['edificioId'];
      this.edificio = this.edificios.find(e => e.ID === this.edificioId);
    });
  }

  edificios: { ID: string, descripcion: string, valor: string, depreciacion: string, mantenimiento: string }[] = [
    { ID: '1', descripcion: 'NuevaContra2', valor: 'Angel Mero Celorio', depreciacion: 'Angel Mero Celorio', mantenimiento: 'Angel Mero Celorio'},
    { ID: '2', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '3', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '4', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '5', descripcion: 'DSADSDA', valor: 'DdsSAD', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '6', descripcion: 'DSADSDA', valor: 'ddasdsa', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '7', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '8', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '9', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '10', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '11', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '12', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '13', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '14', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '15', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
    { ID: '16', descripcion: 'DSADSDA', valor: 'DSADSA', depreciacion: 'DSADA', mantenimiento: 'DASDA'},
  ];
}
