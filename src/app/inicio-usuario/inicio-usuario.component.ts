import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../AuthService';

@Component({
  selector: 'app-inicio-usuario',
  templateUrl: './inicio-usuario.component.html',
  styleUrls: ['./inicio-usuario.component.css']
})
export class InicioUsuarioComponent implements OnInit {
  usuarioAutenticado: { usuario: string; nombreCompleto: string };

  constructor(private router: Router, private authService: AuthService) {
    this.usuarioAutenticado = { usuario: '', nombreCompleto: '' };
  }

  ngOnInit(): void {
    if (!this.verificarSesion()) {
      return;
    }

    this.obtenerUsuarioAutenticado();
  }

  verificarSesion(): boolean {
    if (!this.authService.isAutenticado()) {
      // No hay sesión iniciada, redirige a la página de inicio de sesión
      this.router.navigate(['/login']);
      return false;
    }

    return true;
  }

  obtenerUsuarioAutenticado(): void {
    this.usuarioAutenticado = this.authService.getUsuarioAutenticado();
  }

  cerrarSesion(): void {
    this.authService.cerrarSesion();
    this.router.navigate(['/login']);
  }
}
