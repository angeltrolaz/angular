import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../AuthService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  usuario: string;
  contrasena: string;

  usuarios: { usuario: string, contrasena: string, nombreCompleto: string }[] = [
    { usuario: 'Angel11', contrasena: 'NuevaContra2', nombreCompleto: 'Angel Mero Celorio' },
    { usuario: 'admin', contrasena: 'admin', nombreCompleto: 'Usuario Invitado' }
  ];

  constructor(private router: Router, private authService: AuthService) {
    this.usuario = '';
    this.contrasena = '';
  }

  iniciarSesion() {
    // Obtener los valores de los campos del formulario
    const usuario = this.usuario.trim();
    const contra = this.contrasena.trim();

    // Expresiones regulares y validaciones...

    // Verificar la existencia del usuario
    const usuarioExistente = this.usuarios.find(u => u.usuario === usuario && u.contrasena === contra);

    if (usuarioExistente) {
      // Marcar al usuario como autenticado en el servicio AuthService
      this.authService.setAutenticado(true);
      this.authService.setUsuarioAutenticado(usuarioExistente.usuario, usuarioExistente.nombreCompleto);

      // Redirigir a la página de inicio de usuario
      this.router.navigate(['/facultades']);
    } else {
      // Mostrar un mensaje de error o realizar otra acción
      alert('Credenciales incorrectas');
    }
  }
}
